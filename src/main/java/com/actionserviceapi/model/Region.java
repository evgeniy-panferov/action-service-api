package com.actionserviceapi.model;

import lombok.Data;

@Data
public class Region {

    private String name;
}
