package com.actionserviceapi.model;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class Partner {

    private Long id;

    private String name;

    private String imageUrl;

    private List<Action> actions;

    private LocalDateTime lastUpdate;

    @Override
    public String toString() {
        return "Partner{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", lastUpdate=" + lastUpdate +
                '}';
    }
}
