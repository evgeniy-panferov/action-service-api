package com.actionserviceapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActionServiceApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActionServiceApiApplication.class, args);
	}

}
