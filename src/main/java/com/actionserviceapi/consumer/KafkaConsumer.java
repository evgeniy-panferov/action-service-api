package com.actionserviceapi.consumer;

import com.actionserviceapi.model.Region;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class KafkaConsumer {

    @KafkaListener(topics = "test", groupId = "group_json", containerFactory = "kafkaListenerContainerFactory")
    public void listenMessage(Region region) {
        log.info("region - {}", region);
    }

}
